#pragma once
#include "squirrel.h"
#include "raknet-mini/TCPInterface.h"
#include "raknet-mini/RakNetTypes.h"

class CSquirrelSockets;
class CSquirrelSocketConnectionsMgr;

#ifndef MAX_SOCKETS
	#define MAX_SOCKETS 64
#endif

using namespace RakNet;

class CSquirrelSocketManager
{
public:
	static CSquirrelSockets*				New						( void );
	static CSquirrelSockets*				New						( unsigned char uc );

	static CSquirrelSockets*				Find					( unsigned char uc );

	static bool								Remove					( CSquirrelSockets* p );
	static bool								Remove					( unsigned char uc );

	static void								RemoveAll				( void );

	static unsigned char					FindFreeID				( void );

	static unsigned char					Count					( void )					{ return m_ucSockets; }

	// Functions
	static void								ProcessSockets			( void );

private:
	static unsigned char					m_ucSockets;
	static CSquirrelSockets*				m_Sockets[ MAX_SOCKETS ];
};

class CSquirrelSockets
{
public:
	CSquirrelSockets(void);
	CSquirrelSockets(unsigned char uc);
	~CSquirrelSockets(void);

	unsigned char							GetID(void) const				{ return m_ucID; }

	bool									GetStarted(void) const				{ return m_bStarted; }
	bool									IsServer(void) const				{ return m_bServer; }
	RakNet::TCPInterface*					GetInterface(void) const				{ return m_pInterface; }

	/// \summary Connects to a TCP server
	bool									Connect(const char* szHost, unsigned short usPort);

	/// \summary Starts a TCP server
	bool									Start(unsigned short usPort, unsigned short usMaxConns);
	/// \summary Kills the TCP server
	void									Stop(void);

	void									Send(char* sz, unsigned char ucConn = 255);

	void									CloseConnection(unsigned char ucConn);

	void									SetFunction(char* sz)				{ if (strlen(sz) < sizeof(m_szFunction)) strcpy(m_szFunction, sz); }
	void									SetNewConnFunction(char* sz)				{ if (strlen(sz) < sizeof(m_szNewConnFunc)) strcpy(m_szNewConnFunc, sz); }
	void									SetLostConnFunction(char* sz)				{ if (strlen(sz) < sizeof(m_szLostConnFunc)) strcpy(m_szLostConnFunc, sz); }

	void									Process(void);

	void SqSend(char* sz) { this->Send(sz, 255); }
	void SqSendClient(char* sz, SQInteger ucConn) { this->Send(sz, ucConn); }
	void DeleteMe() { CSquirrelSocketManager::Remove(this); }

private:
	unsigned char							m_ucID;

	bool									m_bStarted;
	bool									m_bServer;
	RakNet::TCPInterface*					m_pInterface;
	CSquirrelSocketConnectionsMgr*			m_pConnMgr;
	SystemAddress							m_sysAddr;

	char									m_szFunction[128];
	char									m_szNewConnFunc[128];
	char									m_szLostConnFunc[128];
};

class CSquirrelSocketConnectionsMgr
{
public:
	CSquirrelSocketConnectionsMgr();
	~CSquirrelSocketConnectionsMgr() {};

	unsigned char					New						( SystemAddress sysAddr );

	unsigned char					Find					( SystemAddress sysAddr );
	SystemAddress					Find					( unsigned char uc );

	bool							Remove					( SystemAddress sysAddr );
	bool							Remove					( unsigned char uc );

	void							RemoveAll				( void );

	unsigned char					FindFreeID				( void );

	unsigned char					Count					( void )					{ return m_ucConnections; }

private:
	unsigned char					m_ucConnections;
	SystemAddress					m_Connections[ 128 ];
};